"""
Trigger LED with button
"""

from time import sleep
from gpiozero import LED, Button
from signal import pause

red = LED(5)
yellow = LED(13)
green = LED(26)
button = Button(21)

# Never ends
while True:
    red.toggle()
    button.wait_for_press()
    sleep(2)
    red.toggle()
    green.toggle()
    sleep(2)
    green.toggle()
    yellow.blink(0.5,0.5)
    sleep(2)
    yellow.toggle()

