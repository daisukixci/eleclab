"""
Trigger LED with button
"""

from time import sleep
from gpiozero import LED, Button
from signal import pause

led = LED(17)
button = Button(13)

# Never ends
#while True:
#    button.wait_for_press()
#    led.toggle()
#    sleep(0.5)

# Ends after one trigger
#button.wait_for_press()
#led.on()
#sleep(3)
#led.off()

# Toggle when pressed
button.when_pressed = led.on
button.when_released = led.off

pause()
