#!/usr/bin/env python3
"""
File: worldclock.py
Author: DaisukiXCI
Email: daisuki@tuxtrooper.com
Github: https://github.com/daisukixci
Description: Personal worlclock project to have time on 5 differents timezone
"""

import datetime
import signal
import sys
import time

import pigpio
import pytz
import tm1637
import i2clcd

import ir_hasher


class WorldClock:
    """
    Control of the WorldClock project
    """

    def __init__(self, clocks_conf, ir_gpio):
        """
        Init the WorldClock

        :param clocks_conf dict: Description of clocks by id linked to clk and dio
        """
        self.clocks = {}
        for clock_id, clock in clocks_conf.items():
            self.clocks[clock_id] = tm1637.TM1637(clk=clock["clk"], dio=clock["dio"])
        signal.signal(signal.SIGINT, self.shutdown)
        signal.signal(signal.SIGTERM, self.shutdown)
        self.ir_commands = {
            2209067173: "channel_less",
            2475776301: "channel",
            811016773: "channel_more",
            22461621: "prev",
            1036916573: "next",
            2338142909: "play",
            383079973: "vol_down",
            2749130309: "vol_up",
            4158662973: "eq",
            2266238925: "0",
            3502206629: "100",
            3131250093: "200",
            1902227973: "1",
            435909485: "2",
            2736323565: "3",
            430130277: "4",
            3072262781: "5",
            3890174357: "6",
            2890417149: "7",
            2654424341: "8",
            727043261: "9",
        }
        self.pi = pigpio.pi()
        self.ir = ir_hasher.hasher(self.pi, ir_gpio, self.callback, 5)
        self.brightness = 7
        self.lcd = i2clcd.i2clcd(i2c_bus=1, i2c_addr=0x27, lcd_width=16)
        self.lcd.init()
        self.lcd_backlight = True

    def display_time(self, clock_id, timezone="Europe/Paris"):
        """
        Display actual time in timezone on clock with ID clock_id

        :param clock_id int: Clock ID you want to display time on
        :param timezone str: Timezone
        """
        now = datetime.datetime.now(tz=pytz.timezone(timezone))
        self.clocks[clock_id].numbers(int(now.hour), int(now.minute))

    def set_brightness(self, brightness=7):
        """
        Set brightness of 7 segments clocks

        :param brightness int: brightness between 0 and 7 included
        """
        for _, clock in self.clocks.items():
            clock.brightness(val=brightness)
            self.brightness = brightness

    def shutdown(self, signum, frame):
        for _, clock in self.clocks.items():
            clock.write([0, 0, 0, 0])
        self.pi.stop()
        self.lcd.clear()
        self.lcd_set_backlight(False)
        sys.exit(0)

    def callback(self, hash):
        if hash not in self.ir_commands:
            return
        print(f"key={self.ir_commands[hash]} hash={hash}")

        if self.ir_commands[hash] == "vol_down":
            self.brightness -= 1
            if self.brightness - 1 < 0:
                self.brightness = 0
            self.set_brightness(self.brightness)
        if self.ir_commands[hash] == "vol_up":
            self.brightness += 1
            if self.brightness + 1 > 7:
                self.brightness = 7
            self.set_brightness(self.brightness)

    def lcd_write(self, message, line=0, align="LEFT"):
        self.lcd.print_line(message, line=line, align=align)

    def lcd_set_backlight(self, status):
        self.lcd_backlight = status
        self.lcd.set_backlight(status)

    def run(self):
        while True:
            self.display_time(1, "America/New_York")
            self.display_time(2, "US/Central")
            self.display_time(3, "US/Pacific")
            self.display_time(4, "Europe/Dublin")
            self.display_time(5, "Europe/Paris")
            self.lcd_write("Hello Corp IT")

            time.sleep(10)


if __name__ == "__main__":
    clocks_conf = {
        1: {"clk": 21, "dio": 20},
        2: {"clk": 19, "dio": 26},
        3: {"clk": 6, "dio": 13},
        4: {"clk": 22, "dio": 5},
        5: {"clk": 17, "dio": 27},
    }
    worldclock = WorldClock(clocks_conf, 4)
    worldclock.run()
